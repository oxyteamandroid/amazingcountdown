/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingCountdown Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.countdown;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ingenic.countdown.utils.Util;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Util.REPEATING)) {
            // 启动提示界面
            Intent intentActivity = new Intent(context, AlarmInfoActivity.class);
            intentActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intentActivity);

            context.getSharedPreferences(Util.SP_NAME,
                    Context.MODE_PRIVATE).edit().putString(Util.KEY_STATE, Util.STATE_RESET).commit();
        } else if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            context.getSharedPreferences(Util.SP_NAME,
                    Context.MODE_PRIVATE).edit().clear().commit();
        }
    }
}
