/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  zhengchuanshu<chuanshu.zheng@ingenic.com>
 *
 *  Elf/Watch-Manager Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.countdown.utils;

import android.os.Build;
import android.os.SystemClock;

public class Util {
    // 广播动作
    public static final String REPEATING = "com.ingenic.countdown.repeating";
    public static final String ACTION_ALARMCLOCK = "com.ingenic.alarmclock.ring";
    public static final String ACTION_COUNTDOWN = "com.ingenic.countdown.ring";
    public static final String ACTION_OUT_CALL = "com.ingenic.mobilecenter.action.OUT_CALL";
    public static final String ACTION_INCOMING_CALL = "com.ingenic.mobilecenter.action.INCOMING_CALL";

    public static final String SP_NAME = "countdownsp";
    public static final String KEY_HOUR = "hour";
    public static final String KEY_MINUTE = "minute";
    public static final String KEY_SECOND = "second";
    public static final String KEY_STATE = "state";
    public static final String KEY_TIME = "time";
    public static final String KEY_TIME_IN_MILLS = "timeInMills";

    public static final String STATE_COUNTING = "counting";
    public static final String STATE_PAUSE = "pause";
    public static final String STATE_RESET = "reset";

    public static final String INTENT_EXTRA_SECOND = "seconds";
    public static final String INTENT_EXTRA_TARGET_SECOND = "target_seconds";
    public static final String INTENT_EXTRA_STATE = "state";

    public static boolean isKitKatOrLater() {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public static int caculatRemainSeconds(long targetSeconds) {
        long timeDifference = targetSeconds - SystemClock.elapsedRealtime();
        int remainSeconds = (int) (timeDifference % 1000 > 0 ? timeDifference / 1000 + 1
                : timeDifference / 1000);

        return remainSeconds;
    }
}
