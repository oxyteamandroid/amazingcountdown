/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingCountdown Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.countdown;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenic.countdown.utils.RingInsist;
import com.ingenic.countdown.utils.ScreenListener;
import com.ingenic.countdown.utils.ScreenListener.ScreenStateListener;
import com.ingenic.countdown.utils.Util;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.RightScrollView;

public class AlarmInfoActivity extends RightScrollActivity implements
        OnClickListener {

    // 闹铃开始和停止的广播ACTION
    private static final String ACTION_STOP = "com.ingenic.clock.stop";
    private static final String ACTION_START = "com.ingenic.clock.start";

    // 延时消息
    private final int MSG_DELAY = 1;
    private final int MSG_TIMEOUT = 2;
    // 锁屏监听
    ScreenListener mListener = new ScreenListener(this);
    AlarmClockReceiver mAlarmClockReceiver = new AlarmClockReceiver();

    // 振动器对象
    private Vibrator mVibrator = null;

    // 确认按钮
    private Button mButton;
    private ImageView mAlarmImage;
    private AnimationDrawable mAnimation = null;

    // 控制屏幕是否常亮
    private PowerManager.WakeLock mWakeLock = null;

    // 用于右滑
    private RightScrollView mView;

    private boolean mFlag = false;
    private Typeface mTf;
    private TextView mText;
    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what) {
                case MSG_DELAY:
                    // 允许右滑
                    if (mView != null) {
                        mView.enableRightScroll();
                    }
                    // 允许点击
                    buttonEnable(true);
                    break;

                default:
                    break;
            }
            return false;
        }
    });

    private void buttonEnable(boolean enable) {
        mButton.setClickable(enable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sendBroadcast(new Intent(Util.ACTION_COUNTDOWN));
        getSharedPreferences(Util.SP_NAME,
                MODE_PRIVATE).edit().putString(Util.KEY_STATE, Util.STATE_RESET).commit();

        // 以下是右滑需要的语句
        mView = getRightScrollView();
        // 初始不允许右滑
        if (mView != null) {
            mView.disableRightScroll();
        }

        mView.setContentView(R.layout.activity_alarm);
        mTf = Typeface.createFromAsset(this.getAssets(),"fonts/fzlthjw.ttf");
        if (null == mAnimation) {
            mAnimation = (AnimationDrawable) getResources().getDrawable(R.anim.alarmanim);
        }
        // 寻找ID
        findViews();

        // 设置控件
        setViews();

        // 初始不允许点击
        buttonEnable(false);
        // 复位

        // 启动振动
        vibratorStart();

        // 启动铃声
        ringStart();

        // 唤醒并点亮屏幕
        wakeUpAndUnlock(AlarmInfoActivity.this);
        // 是否锁屏监听
        if (mListener != null) {
            mListener.begin(new ScreenStateListener() {
                @Override
                public void onUserPresent() {
                }

                @Override
                public void onScreenOn() {
                }

                @Override
                public void onScreenOff() {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            closeandBack();
                        }
                    }, 500);
                }
            });
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(Util.ACTION_ALARMCLOCK);
        filter.addAction(Util.ACTION_OUT_CALL);
        filter.addAction(Util.ACTION_INCOMING_CALL);
        registerReceiver(mAlarmClockReceiver, filter);
        new Thread(new ThreadTimeout()).start();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // 延时，确保动画覆盖下方
        mHandler.sendEmptyMessageDelayed(MSG_DELAY, 1000);
        mAlarmImage.setImageDrawable(mAnimation);
        if(!mAnimation.isRunning()) {
            mAnimation.start();
        }
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (null != mAnimation && mAnimation.isRunning()) {
            mAnimation.stop();
        }
        mAlarmImage.setImageDrawable(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 关闭常亮
        lightOff();

        // 取消振动
        vibratorStop();

        // 取消铃声
        ringStop();

        // 注销屏幕监听器
        if (mListener != null) {
            mListener.unregisterListener();
            mListener = null;
        }
        unregisterReceiver(mAlarmClockReceiver);
    }

    private void findViews() {
        // 按钮ID
        mText = (TextView) findViewById(R.id.textView1);
        mText.setTypeface(mTf);
        mButton = (Button) findViewById(R.id.confirm_button);
        mAlarmImage = (ImageView) findViewById(R.id.alarm_id);
    }

    private void setViews() {
        // 按钮监听
        mButton.setOnClickListener(AlarmInfoActivity.this);

        // 右滑监控
        mView.setOnRightScrollListener(this);
    }

    private void ringStart() {
        Intent intent = new Intent(ACTION_START);
        sendBroadcast(intent);
        try {
            RingInsist.startRing(AlarmInfoActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ringStop() {
        RingInsist.stopRing();
        // 表示是倒计时响铃导致的本此闹钟关闭，不必继续播放音乐
        if(mFlag) return;
        Intent intent = new Intent(ACTION_STOP);
        sendBroadcast(intent);
    }

    /**
     * 开始震动
     */
    private void vibratorStart() {
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // 停止 开启 停止 开启
        long[] pattern = {500, 500, 500, 500};
        // 重复上面的pattern 如果只想震动一次，index设为-1
        mVibrator.vibrate(pattern, 2);
    }

    private void vibratorStop() {
        if (mVibrator != null) {
            mVibrator.cancel();
            mVibrator = null;
        }
    }

    // 解锁并点亮屏幕
    @SuppressWarnings("deprecation")
    private void wakeUpAndUnlock(Context context) {
        KeyguardManager km = (KeyguardManager) context
                .getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = km.newKeyguardLock("unLock");
        // 解锁
        kl.disableKeyguard();
        // 获取电源管理器对象
        PowerManager pm = (PowerManager) context
                .getSystemService(Context.POWER_SERVICE);
        // 获取PowerManager.WakeLock对象,后面的参数|表示同时传入两个值,最后的是LogCat里用的Tag
        mWakeLock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.FULL_WAKE_LOCK, "bright");
        // 点亮屏幕
        mWakeLock.acquire();
    }

    // 关闭屏幕常亮
    private void lightOff() {
        // 释放
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    @Override
    public void onClick(View v) {
        // 只允许点击一次
        buttonEnable(false);
        closeandBack();
    }

    @Override
    public void onRightScroll() {
        closeandBack();
    }

    @Override
    public void onBackPressed() {
        closeandBack();
    }

    private void closeandBack() {
        ((CountDownApplication) getApplication()).finishCountDownActivity();
        finish();
    }

    private class AlarmClockReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Util.ACTION_ALARMCLOCK.equals(action)
                    || Util.ACTION_OUT_CALL.equals(action)
                    || Util.ACTION_INCOMING_CALL.equals(action)) {
                mFlag = true;
                closeandBack();
            }
        }
    }

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == MSG_TIMEOUT) {
                buttonEnable(false);
                closeandBack();
            }
        };
    };

    class ThreadTimeout implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(30*1000);
                Message msg = new Message();
                msg.what = MSG_TIMEOUT;
                handler.sendMessage(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
