/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *  
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *   
 *  Elf/AmazingCountdown Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.countdown;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.NumberPicker.Formatter;
import android.widget.TextView;

import com.ingenic.countdown.utils.Util;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.RightScrollView;

public class CountDownTimerActivity extends RightScrollActivity implements
        Formatter, OnClickListener {
    private final int MSG_DELAY = 1;
    private final int MSG_REVEAL = 2;
    private final int MSG_FLASH = 3;
    private final int MSG_JUMP = 4;
    private final int FIVE_SENCONDS = 5;

    // 用于右滑
    private RightScrollView mView;

    // 倒计时剩余时间
    private TextView numTextView;

    // 取消按钮
    private Button mCancel;

    // 暂停/继续按钮
    private Button mPause;

    // 剩余总秒数
    private int mRemainSeconds = 0;
    private long mTartgetSeconds;
    private String mState;

    // 控制TextView间隔闪烁
    private boolean mChange;

    //暂停
    private boolean mFirstEnable;
    //取消
    private boolean mIsCanceled;
    private Typeface mTf;
    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what) {
                case MSG_DELAY:
                    // 允许滑动
                    if (mView != null) {
                        mView.enableRightScroll();
                    }
                    // 第一界面隐藏，按钮允许点击
                    buttonEnable(true);
                    break;
                case MSG_REVEAL:
                    mRemainSeconds = Util.caculatRemainSeconds(mTartgetSeconds);
                    if (mRemainSeconds <= 0) {
                        mRemainSeconds = 0;
                        countDownTimerCancel();
                        secondsToReveal(mRemainSeconds);
                        buttonEnable(false);
                    } else {
                        secondsToReveal(mRemainSeconds);
                    }
                    mHandler.sendEmptyMessageDelayed(MSG_REVEAL, 200);
                    break;
                case MSG_FLASH:
                    if (!mChange) {
                        numTextView.setVisibility(View.VISIBLE);
                        mChange = true;
                    } else {
                        numTextView.setVisibility(View.INVISIBLE);
                        mChange = false;
                    }
                    mHandler.sendEmptyMessageDelayed(MSG_FLASH, 500);
                    break;
                case MSG_JUMP:
                    // 启动提示界面
                    Intent intentActivity = new Intent(getApplicationContext(), AlarmInfoActivity.class);
                    intentActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentActivity);
                    getSharedPreferences(Util.SP_NAME,
                            MODE_PRIVATE).edit().putString(Util.KEY_STATE, Util.STATE_RESET).commit();
                    break;
                default:
                    break;
            }
            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = getRightScrollView();

        // 初始不允许右滑
        if (mView != null) {
            mView.disableRightScroll();
        }

        mView.setContentView(R.layout.activity_countdown);
        mTf = Typeface.createFromAsset(this.getAssets(),"fonts/fzlthjw.ttf");
        // UI控件初始化
        findViews();
        // UI控件设置
        setViews();
        // 初始不允许点击
        buttonEnable(false);

        //初始化状态和数据
        doRecovery();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mView.setVisibility(View.VISIBLE);
        mHandler.sendEmptyMessageDelayed(MSG_DELAY, 600);

        if (!mFirstEnable) {
            // 显示
            secondsToReveal(mRemainSeconds);
            // 启动定时器
            createCountdownTimer();
        } else {
            recoveryPauseButton();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // 取消定时器
        countDownTimerCancel();
        // 取消闪烁定时器
        flashTimerCancel();

        //如果没有取消并且没有到期则保存在sp中
        if (mRemainSeconds > 0) {
            saveToSp();
            mRemainSeconds = 0;
            mView.setVisibility(View.INVISIBLE);
        }
    }

    private void doRecovery() {
        Intent intent = getIntent();
        mState = intent.getStringExtra(Util.INTENT_EXTRA_STATE);
        mTartgetSeconds = intent.getLongExtra(Util.INTENT_EXTRA_TARGET_SECOND, 0);
        if (mState == null || mState.equals("")) {
            mRemainSeconds = intent.getIntExtra(Util.INTENT_EXTRA_SECOND, 0);
            mFirstEnable = false;
            sendToAlarmOrHandler();
        } else if (mState.equals(Util.STATE_PAUSE)) {
            mRemainSeconds = intent.getIntExtra(Util.INTENT_EXTRA_SECOND, 0);
            mFirstEnable = true;
        } else if (mState.equals(Util.STATE_COUNTING)) {
            mRemainSeconds = Util.caculatRemainSeconds(mTartgetSeconds);
            mFirstEnable = false;
        }
    }

    private void saveToSp() {
        SharedPreferences countdownSp = getSharedPreferences(Util.SP_NAME,
                MODE_PRIVATE);
        SharedPreferences.Editor editor = countdownSp.edit();

        if (mIsCanceled) {
            editor.putString(Util.KEY_STATE, Util.STATE_RESET);
        } else if (mFirstEnable) {
            editor.putString(Util.KEY_STATE, Util.STATE_PAUSE);
        } else {
            editor.putString(Util.KEY_STATE, Util.STATE_COUNTING);
        }

        editor.putInt(Util.KEY_TIME, mRemainSeconds);
        editor.putLong(Util.KEY_TIME_IN_MILLS, mTartgetSeconds);
        editor.commit();
    }

    // 恢复闪烁
    private void recoveryPauseButton() {
        // 显示
        secondsToReveal(mRemainSeconds);
        // 闪烁定时器启动
        createFlashTimer();
        // 换为继续按钮
        //mPause.setText(getResources().getString(R.string.continue_btn));
        mPause.setBackgroundResource(R.drawable.start_selector);
    }

    private void findViews() {

        // 获取TextView ID
        numTextView = (TextView) findViewById(R.id.num_textView);
        numTextView.setTypeface(mTf);
        // 初始化按钮控件
        mCancel = (Button) findViewById(R.id.cancel_button);
        mPause = (Button) findViewById(R.id.pause_button);

    }

    private void setViews() {
        // 取消和暂停按钮监听
        mCancel.setOnClickListener(CountDownTimerActivity.this);
        mPause.setOnClickListener(CountDownTimerActivity.this);

        // 右滑监控
        mView.setOnRightScrollListener(this);
    }

    // 格式化用，使得输入的为0-9的值变为00-09,大于9的则不变
    @Override
    public String format(int value) {
        String tmpStr = String.valueOf(value);
        if (value < 10) {
            tmpStr = "0" + tmpStr;
        }
        return tmpStr;
    }

    // 显示倒计时剩余时间
    private void secondsToReveal(int seconds) {
        numTextView.setVisibility(View.VISIBLE);
        numTextView.setText(format(seconds / 60 / 60) + " : "
                + format(seconds / 60 % 60) + " : " + format(seconds % 60));
    }

    // 启动定时器
    private void createCountdownTimer() {
        mHandler.sendEmptyMessage(MSG_REVEAL);
    }

    // 取消定时器
    private void countDownTimerCancel() {
        if (mHandler.hasMessages(MSG_REVEAL))
            mHandler.removeMessages(MSG_REVEAL);
    }

    // 启动闪烁定时器
    private void createFlashTimer() {
        mHandler.removeMessages(MSG_FLASH);
        mHandler.obtainMessage(MSG_FLASH).sendToTarget();
    }

    // 取消闪烁定时器
    private void flashTimerCancel() {
        mHandler.removeMessages(MSG_FLASH);
    }

    private void sendToAlarmOrHandler() {
        if (mRemainSeconds < FIVE_SENCONDS && Util.isKitKatOrLater()) {
            mHandler.sendEmptyMessageDelayed(MSG_JUMP, mRemainSeconds * 1000);
        } else {
            sendToAlarmReceiver();
        }
    }

    private void cancelAlarm() {
        mHandler.removeMessages(MSG_JUMP);
        cancelAlarmReceiver();
    }

    @SuppressLint("NewApi")
    private void sendToAlarmReceiver() {
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.setAction(Util.REPEATING);
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        PendingIntent sender = PendingIntent.getBroadcast(
                CountDownTimerActivity.this, 0, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        //4.4以上调用setExact方法
        if (Util.isKitKatOrLater())
            am.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, mTartgetSeconds, sender);
        else
            am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, mTartgetSeconds, sender);
    }

    private void cancelAlarmReceiver() {
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.setAction(Util.REPEATING);
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        PendingIntent sender = PendingIntent.getBroadcast(
                CountDownTimerActivity.this, 0, intent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.cancel(sender);
    }

    // 点击按钮回调函数
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_button:
                mIsCanceled = true;
                buttonEnable(false);
                if (!mFirstEnable) {
                    // 取消后台广播
                    cancelAlarm();
                }
                // 取消定时器
                countDownTimerCancel();
                // 取消闪烁定时器
                flashTimerCancel();
                finish();
                break;
            case R.id.pause_button:
                if (!mFirstEnable) {
                    if (mTartgetSeconds - SystemClock.elapsedRealtime() < 500)
                        return;
                    // 取消计时定时器
                    countDownTimerCancel();

                    // 启用闪烁定时器
                    createFlashTimer();

                    // 取消后台广播
                    cancelAlarm();

                    mFirstEnable = true;

                    // 计时停止，变为继续按钮
                    //mPause.setText(getResources().getString(R.string.continue_btn));
                    mPause.setBackgroundResource(R.drawable.start_selector);
                } else {
                    mTartgetSeconds = SystemClock.elapsedRealtime() + mRemainSeconds * 1000;
                    // 取消闪烁定时器
                    flashTimerCancel();

                    // 启动后台广播
                    sendToAlarmOrHandler();

                    // 启动定时器
                    createCountdownTimer();

                    mFirstEnable = false;

                    numTextView.setVisibility(View.VISIBLE);
                    // 计时开始，变为暂停按钮
                    //mPause.setText(getResources().getString(R.string.pause_btn));
                    mPause.setBackgroundResource(R.drawable.stop_selector);
                }
                break;
            default:
                break;
        }

    }

    private void buttonEnable(boolean enable) {
        mCancel.setClickable(enable);
        mPause.setClickable(enable);
    }

    @Override
    public void onBackPressed() {
        ((CountDownApplication) getApplication()).exit();
    }

    @Override
    public void onRightScroll() {
        onBackPressed();
    }
}