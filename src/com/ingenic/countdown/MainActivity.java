/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  ZhanZengYu<zengyu.zhan@ingenic.com>
 *
 *  Elf/AmazingCountdown Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.countdown;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.NumberPicker.Formatter;
import android.widget.NumberPicker.OnScrollListener;
import android.widget.NumberPicker.OnValueChangeListener;

import com.ingenic.countdown.utils.Util;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.RightScrollView;

/**
 * 倒计时程序入口
 *
 * @author ZhanZengYu
 */
public class MainActivity extends RightScrollActivity implements Formatter,
        OnValueChangeListener, OnClickListener, OnScrollListener {

    private final int ZERO = 0, HOUR_MAX = 23, MINUTE_MAX = 59;
    private final int MSGDELAY = 1;

    /**
     * 用于右滑
     */
    private RightScrollView mView;

    // 共享参数相关用于保存上次设置的倒计时时间
    private SharedPreferences countdownSp;

    // 通过NumberPicker获取到的总秒数
    private int mTotalSeconds = 0;

    // 时
    private int mHour = 0;
    // 分
    private int mMinute = 1;
    // 秒
    private int mSecond = 0;

    private Button mStartTime;
    private Button mResetTime;
    private Typeface mTf;
    // 分别用于获取时、分、秒
    private NumberPicker mHourPicker, mMinutePicker, mSecondPicker;

    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MSGDELAY:
                    // 允许右滑
                    if (mView != null) {
                        mView.enableRightScroll();
                    }
                    // 允许点击按钮
                    buttonEnable(true);
                    break;

                default:
                    break;
            }
            return false;
        }
    });

    private void buttonEnable(boolean enable) {
        mStartTime.setEnabled(enable);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jumpToCountdown();
        mView = getRightScrollView();
        if (mView != null) {
            mView.disableRightScroll();
        }

        setTheme(R.style.Transparent);

        mView.setContentView(R.layout.activity_main);
        mTf = Typeface.createFromAsset(this.getAssets(),"fonts/fzlthjw.ttf");

        findViews();

        setViews();

        buttonEnable(false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        jumpToCountdown();
    }

    private void jumpToCountdown() {
        if (countdownSp == null)
            countdownSp = MainActivity.this.getSharedPreferences(Util.SP_NAME,
                    MODE_PRIVATE);
        String state = countdownSp.getString(Util.KEY_STATE, Util.STATE_RESET);
        if (!state.equals(Util.STATE_RESET)) {
            int seconds = countdownSp.getInt(Util.KEY_TIME, 0);
            long targetMillisecond = countdownSp.getLong(Util.KEY_TIME_IN_MILLS, 0);
            Intent intent = new Intent(MainActivity.this,
                    CountDownTimerActivity.class);
            intent.putExtra(Util.INTENT_EXTRA_SECOND, seconds);
            intent.putExtra(Util.INTENT_EXTRA_TARGET_SECOND, targetMillisecond);
            if (state.equals(Util.STATE_PAUSE)) {
                intent.putExtra(Util.INTENT_EXTRA_STATE, Util.STATE_PAUSE);
            } else if (state.equals(Util.STATE_COUNTING)) {
                intent.putExtra(Util.INTENT_EXTRA_STATE, Util.STATE_COUNTING);
            }
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.sendEmptyMessageDelayed(MSGDELAY, 200);
    }

    private void findViews() {
        mStartTime = (Button) findViewById(R.id.ok_button);
        mResetTime = (Button) findViewById(R.id.reset_button);
        mHourPicker = (CustomNumberPicker) findViewById(R.id.hour_picker);
        mMinutePicker = (CustomNumberPicker) findViewById(R.id.minute_picker);
        mSecondPicker = (CustomNumberPicker) findViewById(R.id.second_picker);
    }
    private void setViews() {
        mStartTime.setOnClickListener(MainActivity.this);
        mResetTime.setOnClickListener(this);

        mHour = countdownSp.getInt(Util.KEY_HOUR, 0);
        mMinute = countdownSp.getInt(Util.KEY_MINUTE, 1);
        mSecond = countdownSp.getInt(Util.KEY_SECOND, 0);

        // 时NumberPicker初始化
        numPickerInitial(mHourPicker, HOUR_MAX, ZERO, mHour);

        // 分NumberPicker初始化
        numPickerInitial(mMinutePicker, MINUTE_MAX, ZERO, mMinute);

        // 秒NumberPicker初始化
        numPickerInitial(mSecondPicker, MINUTE_MAX, ZERO, mSecond);

        mHourPicker.setOnScrollListener(this);
        mMinutePicker.setOnScrollListener(this);
        mSecondPicker.setOnScrollListener(this);

        buttonState();
    }

    private void numPickerInitial(NumberPicker numberPicker, int max, int min,
                                  int now) {
        // 设置NumberPicker的显示格式，使得0-9显示为00-09
        numberPicker.setFormatter(MainActivity.this);
        // 监听NumberPicker的值是否发生改变
        numberPicker.setOnValueChangedListener(MainActivity.this);
        numberPicker.setMaxValue(max);
        numberPicker.setMinValue(min);
        numberPicker.setValue(now);
        // 以下三句使得NumberPicker无法点击输入
        numberPicker
                .setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    }

    @Override
    public void onRightScroll() {
        super.onBackPressed();
    }

    // 格式化用，使得输入的为0-9的值变为00-09,大于9的则不变
    @Override
    public String format(int value) {
        String tmpStr = String.valueOf(value);
        if (value < 10) {
            tmpStr = "0" + tmpStr;
        }
        return tmpStr;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        // 判断是哪个NumberPicker的值发生改变
        switch (picker.getId()) {
            // 时
            case R.id.hour_picker:
                mHour = newVal;
                break;
            // 分
            case R.id.minute_picker:
                mMinute = newVal;
                break;
            // 秒
            case R.id.second_picker:
                mSecond = newVal;
                break;
            default:
                break;
        }

        buttonState();
    }

    private void buttonState() {
        if (mHour != 0 || mMinute != 0 || mSecond != 0) {
            mStartTime.setEnabled(true);
            //mStartTime.setBackgroundResource(R.drawable.start_selector);
        } else {
            // 都为0时按钮不可点击
            mStartTime.setEnabled(false);
            //mStartTime.setBackgroundResource(R.drawable.start_selector);
        }
    }

    // 由时、分、秒获得总秒数
    private void numPickerSeconds(int hour, int minute, int second) {
        mTotalSeconds = ((hour * 60 + minute) * 60 + second);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok_button:
                //buttonEnable(false);

                if (mView != null) {
                    mView.disableRightScroll();
                }
                Editor editor = countdownSp.edit();
                editor.putInt(Util.KEY_HOUR, mHour);
                editor.putInt(Util.KEY_MINUTE, mMinute);
                editor.putInt(Util.KEY_SECOND, mSecond);
                editor.commit();
                // 转换为总秒数
                numPickerSeconds(mHour, mMinute, mSecond);

                Intent intent = new Intent(MainActivity.this,
                        CountDownTimerActivity.class);
                intent.putExtra(Util.INTENT_EXTRA_SECOND, mTotalSeconds);
                intent.putExtra(Util.INTENT_EXTRA_TARGET_SECOND, SystemClock.elapsedRealtime() + mTotalSeconds * 1000);
                startActivity(intent);
                break;

            case R.id.reset_button:
                mHour = 0;
                mMinute = 0;
                mSecond = 0;

                mHourPicker.setValue(mHour);
                mMinutePicker.setValue(mMinute);
                mSecondPicker.setValue(mSecond);

                mStartTime.setEnabled(false);
                //mStartTime.setBackgroundResource(R.drawable.start_selector);
                break;

            default:
                break;
        }
    }

    @Override
    public void onScrollStateChange(NumberPicker view, int scrollState) {
        switch (scrollState) {
            case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
            case OnScrollListener.SCROLL_STATE_FLING:
                //mStartTime.setEnabled(false);
                break;
            case OnScrollListener.SCROLL_STATE_IDLE:
                //buttonState();
                break;
            default:
                break;
        }
    }
}
